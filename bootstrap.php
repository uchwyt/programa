<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 09:48
 */
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once 'vendor/autoload.php';

$config = Setup::createAnnotationMetadataConfiguration( [ __DIR__ . "/src" ], true );

$conn = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/db.sqlite',
);

$entityManager = EntityManager::create( $conn, $config );
