const escape = str => str.replace(/&/g, '&amp;')
                         .replace(/"/g, '&quot;')
                         .replace(/'/g, '&#x27;')
                         .replace(/</g, '&lt;')
                         .replace(/>/g, '&gt;')
                         .replace(/\//g, '&#x2F;')
                         .replace(/\\/g, '&#x5C;')
                         .replace(/`/g, '&#96;');

const isEmpty = (str, options) => {
  options = {...options, ignoreWhitespace: false};
  return (options.ignoreWhitespace ? str.trim().length : str.length) === 0;
};
const isRequired = value => (!value || (value && isEmpty('' + value)));
const isYoungerThen = (age, value) => (!value || parseInt(value) < age);
const isOlderThen = (age, value) => (!value || parseInt(value) > age);
const isNumeric = value => {
  const numeric = /^[+-]?([0-9]*[.])?[0-9]+$/;

  return !value || !numeric.test(value);
};
const isChecked = checked => !checked;

const validators = {
  name: {required: isRequired},
  lastname: {required: isRequired},
  age: {required: isRequired, numeric: isNumeric, isTooYoung: isYoungerThen.bind(undefined, 18), isTooOld: isOlderThen.bind(undefined, 99)},
  gender: {required: isRequired},
  confirmation: {required: isChecked},
};
const errorStrings = {
  required: 'Pole jest wymagane.',
  numeric: 'Podano nieprawidłowy wiek. Wiek powinien być liczbą naturalną.',
  isTooYoung: `Podany wiek jest poniżej dopuszczalnego, który wynosi 18 lat.`,
  isTooOld: `Podany wiek jest powyżej dopuszczalnego, który wynosi 99 lat.`,
};
const getErrorString = key => errorStrings[key] || key;

const getValidity = (validators, value) => {
  if (typeof validators === 'function') {
    const fieldValue = typeof value === 'string' ? escape(value) : value;

    return validators(fieldValue);
  }

  const keys = Object.keys(validators);
  let errors = {};

  keys.map(key => {
    const error = getValidity(validators[key], value);
    if (error) {
      errors[key] = error;
    }
  });
  return errors;
};

function validateField() {
  const name = this.name;
  const fieldValidators = validators[name];
  if (!fieldValidators) {
    return;
  }
  const parentNode = this.parentNode;
  const errorContainer = parentNode.querySelector('.errors');

  const errors = getValidity(fieldValidators, (this.type === 'checkbox' ? this.checked : this.value));
  if (Object.keys(errors).length > 0) {
    if (!this.classList.contains('invalid')) {
      this.classList.add('invalid');
      this.classList.remove('valid');
    }
    const errorStr = Object.keys(errors).map(key => getErrorString(key)).join(' ');

    if (errorContainer) {
      errorContainer.textContent = errorStr;
    } else {
      const tmpElem = document.createElement('div');
      tmpElem.className = 'errors';
      tmpElem.textContent = errorStr;
      parentNode.appendChild(tmpElem);
    }
  } else {
    if (this.classList.contains('invalid')) {
      this.classList.remove('invalid');
      this.classList.add('valid');
    }
    if (errorContainer) {
      parentNode.removeChild(errorContainer);
    }
  }
}

document.querySelectorAll('form input, form select').forEach(elem => {
  elem.addEventListener('change', validateField);
  validateField.call(elem);
});
