<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 10:14
 */

require_once "bootstrap.php";

use Doctrine\ORM\EntityManager;

class CategoryManager {
    protected $manager = null;

    public function __construct( EntityManager $manager ) {
        $this->manager = $manager;
    }

    public function getAvailableProductsForCategory(Int $idCategory): Int {
        try {
            /** @var Category $category */
            $category = $this->manager->find('Category', $idCategory);
            return $category ? count($category->getProducts()) : 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function getProductList(Int $idCategory, $sort = 'name', $order = 'asc'): Array {
        try {
            /** @var Category $category */
            $category = $this->manager->find( 'Category', $idCategory );
            return $category ? $category->getSortedProducts($sort, $order) : [];
        } catch(Exception $e) {
            return [];
        }
    }
}
