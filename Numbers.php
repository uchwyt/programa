<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 00:05
 */

class Numbers {
    private $added;

    public function __construct() {
        $this->added = [];
    }

    public function getAdded(): Array {
        return $this->added;
    }

    public function addNumber(Int $number): void {
        if($number < 0) {
            throw new Exception('Nieprawidłowa liczba');
        }
        $this->added[] = $number;
    }

    /**
     * @return int -1 - trzykrotność nieparzystych, 0 - po równo, 1 - dwukrotność parzystych
     */
    public function compareSum(): Int {
        $even = 0;
        $odd = 0;
        foreach($this->added as $one) {
            if($one % 2 === 0) {
                $even += $one;
            } else {
                $odd += $one;
            }
        }
        return $even * 3 <=> $odd * 2;
    }
}
