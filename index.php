<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <style>
            *{
                box-sizing:border-box;
            }
            .row{
                display:flex;
                flex-wrap:wrap;
                width:100%;
            }
            .col-4, .col-8, .col-12{
                padding:0 16px;
                flex-grow:0;
            }
            .col-4{
                width:33%;
                flex-basis:33%;
            }
            .col-8{
                width:66%;
                flex-basis:66%;

            }
            .col-12{
                width:100%;
                flex-basis:100%;
            }
            form > div{
                margin-bottom:1.5rem;
            }
            form label + input{
                display:block;
            }
            .valid{
                border-color:green;
            }
            .invalid{
                border-color:red;
            }
            .invalid + .errors{
                color:red;
            }
        </style>
        <title>Programa test app</title>
    </head>
    <body>
        <div class="row">
            <aside class="col-4">
                <div id="menu">
                    <ol>
                        <?php

                        function getMenuItems( Mysqli $connect, $idParent = null ): Array {
                            if ( is_null( $idParent ) ) {
                                $where = ' id_parent IS NULL';
                            }
                            else {
                                $where = ' id_parent = ' . $idParent;
                            }
                            $results = $connect->query( 'SELECT * FROM menu WHERE ' . $where );
                            $rows    = $results->fetch_all( MYSQLI_ASSOC );
                            if ( count( $rows ) ) {
                                foreach ( $rows as &$row ) {
                                    $row[ 'children' ] = getMenuItems( $connect, $row[ 'ID' ] );
                                }
                            }

                            return $rows;
                        }

                        function printMenu( Array $item ): Void {
                            echo '<li>' . $item[ 'label' ];

                            if ( count( $item[ 'children' ] ) ) {
                                echo '<ul>';
                                foreach ( $item[ 'children' ] as $childItem ) {
                                    printMenu( $childItem );
                                }
                                echo '</ul>';
                            }

                            echo '</li>';
                        }

                        $host     = 'localhost';
                        $user     = 'test';
                        $password = 'test';
                        $database = 'test';
                        $connect  = new mysqli( $host, $user, $password, $database, 3306 );
                        if ( $connect->connect_error ) {
                            die( $connect->connect_error );
                        }
                        $menuItems = getMenuItems( $connect );
                        $connect->close();

                        foreach ( $menuItems as $item ) {
                            printMenu( $item );
                        }
                        ?>
                    </ol>
                </div>
            </aside>

            <main class="row col-8">
                <section class="col-8">
                    <h1>
                        Marcin Zamelski
                    </h1>
                    <h2>Full-stack developer</h2>

                    <h3>Summary</h3>
                    <p>
                        Full-stack developer with 3 years of experience in developing web applications. Possess a strong knowledge of both frontend (React) and backend (PHP7, nodeJS). An autonomous worker committed to providing high quality code. Seeking to leverage my technical and professional expertise in new projects.
                    </p>

                    <h3>Experience</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    2015-11 - present
                                </td>
                                <td>
                                    <h5>
                                        Software developer
                                    </h5>
                                    <h6>Funclub sp. z o.o.</h6>
                                    <ul>
                                        <li>Maintenance, update and expansion of company CRM (PHP and MySQL)<br></li>
                                        <li>Building and maintenance of company web apps (<a href="https://www.funclub.pl"
                                                                                             rel="nofollow noopener"
                                                                                             target="_blank">funclub.pl</a>, <a
                                                    href="https://obozymlodziezowe.pl" rel="nofollow noopener"
                                                    target="_blank">obozymlodziezowe.pl</a>, <a href="https://www.sferaplatinum.pl"
                                                                                                rel="nofollow noopener" target="_blank">sferaplatinum.pl</a>
                                            etc.)<br></li>
                                        <li>System administration (Linux, MacOS, Windows Server 2012)</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <h3>Education</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    2012-10 - 2015-07
                                </td>
                                <td>
                                    Adam Mickiewicz University, Poznan, Military History, bachelor degree
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2011-10 - 2012-04
                                </td>
                                <td>
                                    Państwowa Wyższa Szkoła Zawodowa, Leszno, Computer Science, aborted
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2007-09 - 2011-04
                                </td>
                                <td>
                                    Zespół Szkół Technicznych im. 55. Poznańskiego Pułku Piechoty, Leszno, Computer Science, Technician
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <h3>
                        Interests
                    </h3>
                    <p>
                        Programming, Literature, Electronics, Learning, Sport, Walking
                    </p>
                </section>
                <aside class="col-4">
                    <h3>Personal Info</h3>
                    <ul>
                        <li>Phone: +48783772956</li>
                        <li>E-mail: marcin.zamelski@hotmail.com</li>
                        <li>Date of birth: 1991-07-07</li>
                        <li>GitHub: <a href="http://github.com/uchwyt/" target="_blank" rel="nofollow noopener">github.com/uchwyt</a></li>
                        <li>LinkedIn: <a href="http://linkedin.com/in/marcin-zamelski-354752153/" target="_blank" rel="nofollow noopener">linkedin.com/in/marcin-zamelski-354752153</a>
                        </li>
                    </ul>

                    <h3>Skills</h3>
                    <p><b> Good programming skills of </b>PHP 7, JavaScript, React</p>
                    <p><b>Knowledge of </b>jQuery, CSS 3, HTML 5, Wordpress, SASS</p>
                    <p><b>Basic knowledge of</b> Python, PhalconPHP, NodeJs, Webpack, Babel, Git, RestAPI, Material UI, Bootstrap, Jest, ES7<br></p>

                    <h3>Software</h3>
                    <p><b>Basic knowledge of</b> Adobe Photoshop, Gimp</p>

                    <h3>Languages</h3>
                    <ul>
                        <li>Polish - *****</li>
                        <li>English - ****</li>
                        <li>Spanish, Hindi - *</li>
                    </ul>
                </aside>

                <section class="col-12">
                    <h1>Formularz</h1>
                    <form>
                        <div>
                            <label for="name">Imię</label>
                            <input name="name" id="name" type="text">
                        </div>
                        <div>
                            <label for="lastname">Nazwisko</label>
                            <input name="lastname" id="lastname" type="text">
                        </div>
                        <div>
                            <label for="age">Wiek</label>
                            <input name="age" id="age" type="number">
                        </div>
                        <div>
                            <label for="gender">Płeć</label>
                            <select name="gender" id="gender">
                                <option value=""></option>
                                <option value="man">Mężczyzna</option>
                                <option value="woman">Kobieta</option>
                            </select>
                        </div>
                        <div>
                            <label>
                                <input name="confirmation" id="confirmation" type="checkbox">
                                Wyrażam zgodę na przetwarzanie moich danych osobowych przez...
                            </label>
                        </div>
                        <div>
                            <input type="submit" value="Wyślij"/>
                        </div>
                    </form>
                </section>
            </main>
        </div>
        <script type="application/javascript" src="js/main.js"></script>
    </body>
</html>
