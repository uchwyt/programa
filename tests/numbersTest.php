<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 00:05
 */

use PHPUnit\Framework\TestCase;

require_once('../Numbers.php');

class numbersTest extends TestCase {
    public function testNumberCanBeAdded(): void {
        $number = new Numbers();
        $number->addNumber( 1 );
        $this->assertEquals( 1, count( $number->getAdded() ), );
    }

    public function testNegativeNumberCannotBeAdded(): void {
        $number = new Numbers();

        $this->expectException(Exception::class);

        $number->addNumber( - 5 );
    }

    public function testTwiceOfOddIsBigger(): void {
        $number = new Numbers();
        $arr    = [
            66,
            93,
            57,
            77,
            27,
            71,
        ];
        foreach ( $arr as $one ) {
            $number->addNumber( $one );
        }

        $this->assertEquals( -1, $number->compareSum() );
    }

    public function testThreeTimesOfEvenIsBigger(): void {
        $number = new Numbers();
        $arr    = [
            67,
            92,
            56,
            72,
            27,
            72,
        ];
        foreach ( $arr as $one ) {
            $number->addNumber( $one );
        }

        $this->assertEquals( 1, $number->compareSum() );
    }

    public function testResultsAreEqual(): void {
        $number = new Numbers();
        $arr    = [
            7,
            17,
            43,
            11,
            6,
            16,
            20,
            10,
        ];
        foreach ( $arr as $one ) {
            $number->addNumber( $one );
        }

        $this->assertEquals( 0, $number->compareSum() );
    }
}
