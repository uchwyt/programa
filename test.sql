SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci;
USE `test`;

CREATE TABLE `menu` (
  `ID` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `id_parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

INSERT INTO `menu` (`ID`, `label`, `id_parent`) VALUES
(1, 'Kategoria główna', NULL),
(2, 'Podkategoria 1', 1),
(3, 'Podkategoria 2', 1),
(4, 'Podkategoria 1.1', 2),
(5, 'Podkategoria 1.2', 2),
(6, 'Podkategoria 2.1', 3),
(7, 'Podkategoria 2.2', 3);


ALTER TABLE `menu` ADD PRIMARY KEY (`ID`);

ALTER TABLE `menu` MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;
