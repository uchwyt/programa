<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 10:05
 */

use Doctrine\Common\Collections\Criteria;

/**
 * @Entity @Table(name="categories")
 **/
class Category {
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string") * */
    protected $name;

    /**
     * Many categories can have many products
     * @var Product[]
     * @ManyToMany(targetEntity="Product")
     */
    protected $products;

    public function __construct() {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): Int {
        return $this->id;
    }

    /**
     * @return Product[]
     */
    public function getProducts() {
        return $this->products->getValues();
    }

    public function getSortedProducts($sort = 'name', $order = 'asc') {
        if(strtolower($order) !== 'asc' && strtolower( $order ) !== 'desc') {
            $order = 'ASC';
        }
        print_r( [ $sort => strtoupper( $order ) ]);
        $criteria = Criteria::create()->orderBy( [ $sort => strtoupper( $order ) ]);
        return $this->products->matching($criteria)->getValues();
    }

    /**
     * @param Product $product
     */
    public function assignToProduct(Product $product): void {
        $this->products[] = $product;
    }
}
