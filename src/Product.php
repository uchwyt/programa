<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 09:53
 */

/**
 * @Entity @Table(name="products")
 **/
class Product {
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string") * */
    protected $name;

    /** @Column(type="text", nullable=true) * */
    protected $description;

    /** @Column(type="float") * */
    protected $price;

    /** @Column(type="boolean") * */
    protected $availability;

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription( string $description ): void {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isAvailability(): bool {
        return $this->availability;
    }

    /**
     * @param bool $availability
     */
    public function setAvailability( bool $availability ): void {
        $this->availability = $availability;
    }

    /**
     * @return float
     */
    public function getPrice(): float {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice( float $price ): void {
        print_r($price);
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ): void {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }
}
