<?php
/**
 * Created by IntelliJ IDEA.
 * User: marcinek
 * Date: 24.03.19
 * Time: 10:12
 */

require_once "bootstrap.php";

use Doctrine\ORM\EntityManager;

class ProductManager {
    protected $manager = null;

    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
    }

    public function getNumberOfAvailableProducts(): Int {
        $dql = 'SELECT COUNT(p) as productCount FROM Product p WHERE p.availability = 1';
        $query = $this->manager->createQuery($dql);
        try{
            $count = $query->getSingleScalarResult();
            return $count;
        } catch(Exception $e) {
            return 0;
        }
    }

    public function getNotAvailableProducts(): Array {
        $dql   = 'SELECT p FROM Product p WHERE p.availability = 0';
        $query = $this->manager->createQuery( $dql );
        $products = $query->getResult();

        return $products;
    }

    public function getProductsByText(String $name): Array {
        $qb    = $this->manager->createQueryBuilder();
        $query = $qb->select('p')
            ->from('Product', 'p')
            ->where($qb->expr()->like('p.name', '?1'))
            ->setParameter(1, '%' . $name . '%')
            ->getQuery();
        $products = $query->getScalarResult();

        return $products;
    }
}
